package arbol; //materialize
/**@author Valeria Huerta Alvarez*/

public class Arbol {
    Nodo raiz;
    
    public Arbol(){
        raiz = new Nodo();
    }
    
    public Arbol (Nodo nodo){
        raiz = nodo;
    }
    
    public Arbol (Object info){
        raiz = new Nodo(info);
    }
    
    public boolean vacio (){
        /*if (raiz == null)//este sobra
            return true;*/
        if(raiz == null)return true;
        if (raiz.info == null)
            return true;
        return false;
    }
    
    public boolean vacio(Nodo nodo){  //sobreescribir: un metodo dado x el pograma, le definimos un nuevo comportamiento
                                      //sobrecargar: que tenga diferentes parametros.
        if(nodo==null) return true;
        if(nodo.info==null) return true;
        return false;
    }
   
    public boolean insertar (Nodo nodo, Comparable info){ //Recursivo
        if(vacio(nodo)){
            nodo = new Nodo(info);
            return true;
        }else{
            if (info.compareTo(nodo.info)>0)return insertar(nodo.der, info);
            else return insertar(nodo.izq, info);
        }
    }
    
    public boolean insertar (Comparable info){
        Nodo temp = raiz;
        
        if (vacio()){
            raiz.info = info;
            return true;
        }
        while (true){
            if (info.compareTo(temp.info)>0){
                if(temp.der == null){
                    temp.der = new Nodo(info);
                    return true; //si pasa el if (si insertamos), no es necesario que el ciclo continúe
                }
                temp = temp.der;
                
            }else{
                if(temp.izq == null){
                    temp.izq = new Nodo(info);
                    return true;
                }
                temp = temp.izq;
            }
        }
    }
    
    //Encuentra el nodo que tiene ese valor:
    public Nodo buscar(Comparable info){ //para que encuentre lo que queremos eliminar
       Nodo temp = raiz;        
        if (vacio())return null;        
        while (true){
            if (info.equals(temp.info)) return temp; 
            if (info.compareTo(temp.info)>0)temp = temp.der;
            else temp = temp.izq;
        }      
    }
    
    public Nodo buscarMayor(Nodo eliminar){ 
       Nodo tmp = eliminar.izq;                
        while (tmp.der!=null){
            tmp = tmp.der;
        }  //hasta ahora tmp2 = tmp
        Nodo mayor = tmp;
        
        if(tmp.izq!=null)tmp = tmp.izq;
        mayor.der = null;
        mayor.izq = null;
        return mayor;
    }
    
    public boolean eliminar(Comparable info){
        Nodo tmp = buscar(info); //tmp es el que queremos borrar
        if (tmp == null) return false; //No hace nada.
        //Caso 1: Sin hijos.
        if(tmp.izq == null && tmp.der == null)tmp = null;            
        //Caso 2: Sólo hay hijo derecho.
        else if (tmp.izq == null) tmp = tmp.der;
        //Caso 3: Sólo hay hijo izquierdo.
        else if ( tmp.der == null)tmp = tmp.izq;              
        else{
            Nodo mayor = buscarMayor(tmp); //tmp es el que voy a borrar
            mayor.izq = tmp.izq;
            mayor.der = tmp.der;
            tmp = mayor;
        }
        return true;
    }
    
    public String inorden(Nodo nodo){
        if(nodo ==null)return "";
        String res = "";//string es cadena
        res += nodo.info.toString();
        if(nodo.izq!=null){
            res += ", [";
            res += inorden(nodo.izq);
            res += "]";
        }
        if(nodo.der!=null){
            res += ", [";
            res += inorden(nodo.der);
            res += "]";
        }
        return res;
    }
    
    public String preorden(Nodo nodo){//falta completar
        String res ="";
        return res;
    }
    
    public String postorden(Nodo nodo){//falta completar
        String res ="";
        return res;
    }
}

