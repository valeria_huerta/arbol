package arbol;

/**@author Huerta Alvarez Valeria */

public class Nodo {
    Object info;
    Nodo izq;
    Nodo der;
    
    public Nodo(){}
    
    public Nodo (Object info){
        this.info = info;
        izq = null;
        der = null;
    }
    
    public boolean vacio(){
        return(info == null);//si info == null regresa true
    }
    
    public void insertar(Comparable info){
        if(vacio())this.info = info;            
        else if(info.compareTo(this.info)>0){
            if(der==null)  der = new Nodo(info);                
            else  der.insertar(info);   
        }else{
            if (izq==null) izq =new Nodo(info);                
            else izq.insertar(info);                
        }
    }
    
    public Nodo buscarMayor(){ //regresa el nodo mayor de forma recursiva
        //Nodo nodo
        //nodo.buscarMayor;
        if(der == null) return this; //retorna este mismo objeto xq no tiene hijos
        else return der.buscarMayor();
    }
    
    public Nodo buscarMenor(){
        if(izq == null) return this;
        else return izq.buscarMenor();
    }
    
    public void reemplazar(Nodo nodo){
        this.info = nodo.info;
        this.der = nodo.der;
        this.izq = nodo.izq;
    }
    
    public void eliminar(Comparable info){
        if(info.equals(this.info)){ //si la info es = a la que queremos eliminar, ese es el nodo a eliminar
            if(der == null && izq == null) return;//no tiene hijos
               //this = null; return this;
            else if(der == null && izq != null){
                this.info = izq.info;
                this.izq = izq.izq;
                this.der = izq.der;
            }else if( der!= null && izq == null){
                this.info = der.info;
                this.izq = der.izq;
                this.der = der.der;
            }else if(der!= null && izq!= null){
                Nodo tmp = izq.buscarMayor();
                tmp.izq = izq;
                tmp.der = der;
                this.izq = tmp.izq;
                this.der = tmp.der;
                this.info = tmp.info;
            }
        }
    }
}
